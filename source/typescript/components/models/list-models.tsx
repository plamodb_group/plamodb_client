// #region Imports

/* Mobx */
import {observer} from 'mobx-react-lite';

/* React */
import React, {useContext, useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';

/* Material UI */
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import AddIcon from '@material-ui/icons/Add';

import {fade} from '@material-ui/core/styles/colorManipulator';

/* plamodb */
import {Model} from '../../api/contracts';
import ModelsStore from '../../stores/models';

// #endregion Imports

// #region Styles

let styles = makeStyles((theme : Theme) => 
  createStyles({
    card: {
      position:     'relative'
    },

    avatar: {
      height:       0,
      paddingTop:   '56.25%'
    },

    overlay: {
      position:     'absolute',
      bottom:       0,
      left:         0,
      right:        0,
      padding:      theme.spacing(0.25, 1),
      background:   fade(theme.palette.secondary.main, 0.7)
    }
  })
);

// #endregion Styles

function ListModels() {

  // #region Hooks

  const history = useHistory();
  const modelsStore = useContext(ModelsStore);

  useEffect(
    () => {
      modelsStore.loadModels();
    },
    []    // Only run once.
  );

  // #endregion Hooks

  // #region Components

  const classes = styles();

  // #endregion Components

  // #region Events

  // Adds a new model, opens the edit view for it.
  async function onAdd() : Promise<void> {
    let model : Model = await modelsStore.addModel();
    history.push(`/models/${model.id}/edit`);
  }

  // #endregion Events

  return (
    <Box>

      {/* Title / Tools */}
      <AppBar 
        position    = "static" 
        elevation   = {0}
        color       = "primary"
      >
        <Toolbar className="tall">
          <Grid container justify="flex-end">
            <IconButton onClick={onAdd}>
              <AddIcon />
            </IconButton>
          </Grid>
        </Toolbar>
      </AppBar>

      <Box m={4}>
        <Grid container spacing={4}>
          {modelsStore.Models.map((model : Model) => (
            <Grid item key={model.id} xs={12} sm={6} md={3}>

              {/* Model */}
              <Card 
                className   = {classes.card}
                variant     = "elevation"
                elevation   = {0}
              >
                <CardActionArea 
                  component   = {Link} 
                  to          = {`/models/${model.id}`}
                >

                  <CardMedia
                    className   = {classes.avatar}
                    image       = {`/api/assets/${model.avatar}`}
                    title       = {model.name}
                  />

                  <div className={classes.overlay}>
                    <Typography variant="h4" color="primary">{model.name}</Typography>
                    <Typography variant="subtitle1" color="textPrimary">{model.caption}</Typography>
                  </div>

                </CardActionArea>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Box>

    </Box>
      
  );
}

export default observer(ListModels);