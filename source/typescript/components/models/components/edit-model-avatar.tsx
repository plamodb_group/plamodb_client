// #region Imports

/* React */
import React, {ChangeEvent, useContext, useRef} from 'react';

/* Material UI */
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';

/* plamodb */
import {Model} from '../../../api/contracts';
import ModelsStore from '../../../stores/models';
import default_avatar_png from '../../../../images/default_avatar.png';

// #endregion Imports

// #region Styles

let styles = makeStyles((theme : Theme) => 
  createStyles({
    avatar: {
      width:      '50%'
    }
  })
);

// #endregion Styles

function EditModelAvatar(props : {model : Model}) {
  
  // #region Hooks

  const uploadInputRef = useRef(null);
  const modelsStore = useContext(ModelsStore);

  // #endregion Hooks

  // #region Components

  let classes = styles();
  let avatar = props.model?.avatar ? '/api/assets/' + props.model.avatar : default_avatar_png;

  // #endregion Components

  // #region Events

  function onFileSelected(event : ChangeEvent<HTMLInputElement>) {
    modelsStore.setAvatar(props.model.id, event.target.files[0]);
  }

  // #endregion Events

  return (
    <Grid container>

      <Grid 
        container
        item
        xs      = {12}
        justify = "center"
      >
        <img 
          className     = {classes.avatar}
          src           = {avatar}
        />
      </Grid>

      <Grid 
        container 
        item 
        xs      = {12} 
        justify = "flex-end"
      >

        {/* Upload */}
        <Button 
          type        = "submit"
          variant     = "contained" 
          color       = "primary"
          onClick     = {() => uploadInputRef.current && uploadInputRef.current.click()}
        >
          Upload
        </Button>

        <input
          ref       = {uploadInputRef}
          type      = "file"
          accept    = "image/*"
          style     = {{ display: "none" }}
          onChange  = {onFileSelected}
        />

      </Grid>

    </Grid>
  );
} 

export default EditModelAvatar;