// #region Imports

/* React */
import React, {useContext} from 'react';

/* Material UI */
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

/* Formik */
import {useFormik} from 'formik';

/* plamodb */
import {Model} from '../../../api/contracts';
import ModelsStore from '../../../stores/models';

// #endregion Imports

// #region Form

interface FormFields { 
  name : string;
  caption : string;
  description : string;
  releasedOn : string;
};

// #endregion Form

function EditModelProperties(props : {model : Model}) {

  // #region Hooks

  const formik = useFormik<FormFields>({
    initialValues:        ToFormFields(props.model),
    enableReinitialize:   true,
    validateOnMount:      true,
    validate:             validate,
    onSubmit:             (values : FormFields) => {
      modelsStore.updateModel(props.model.id, ToModel(values, props.model));
    },
  });

  const modelsStore = useContext(ModelsStore);

  // #endregion Hooks

  return (
    <Grid 
      container
      spacing     = {2}
      component   = "form"
      onSubmit    = {formik.handleSubmit}
    >

      <Grid item xs={12}>

        {/* Name */}
        <TextField 
          name        = "name"
          label       = "Name"
          variant     = "filled"
          type        = "text"
          required
          fullWidth
          error       = {formik.touched.name && !!formik.errors.name}
          {...formik.getFieldProps('name')}
        >
        </TextField>

      </Grid>

      <Grid item xs={12}>

        {/* Caption */}
        <TextField 
          name        = "caption"
          label       = "Caption"
          variant     = "filled"
          type        = "text"
          required
          fullWidth
          error       = {formik.touched.caption && !!formik.errors.caption}
          {...formik.getFieldProps('caption')}
        >
        </TextField>
        
      </Grid>
      
      <Grid item xs={12}>

        {/* Description */}
        <TextField
          name        = "description"
          label       = "Description"
          variant     = "filled"
          type        = "text"
          multiline
          rows        = {6}
          required
          fullWidth
          error       = {formik.touched.description && !!formik.errors.description}
          {...formik.getFieldProps('description')}
        >
        </TextField>

      </Grid>

      <Grid item xs={6}>

        {/* Released On */}
        <TextField 
          name        = "releasedOn"
          label       = "Released On"
          variant     = "filled"
          type        = "date"
          required
          fullWidth
          error       = {formik.touched.releasedOn && !!formik.errors.releasedOn}
          helperText  = {formik.errors.releasedOn}
          {...formik.getFieldProps('releasedOn')}
        >
        </TextField>

      </Grid>

      <Grid 
        container 
        item 
        xs      = {12} 
        justify = "flex-end"
      >
        
        {/* Save Changes */}
        <Button 
          type        = "submit"
          variant     = "contained" 
          color       = "primary"
          disabled    = {!formik.isValid}
        >
          Save
        </Button>

      </Grid>

    </Grid>
  );
}

export default EditModelProperties;

// #region Utilities

function validate(values : FormFields) {
  const errors : any = {};

  if (!values.name || values.name.length === 0) {
    errors.name = 'Name is required.';
  }

  if (!values.description || values.description.length === 0) {
    errors.description = 'A description is required.';
  }

  if (!values.releasedOn || values.releasedOn.length === 0) {
    errors.releasedOn = 'A release date is required.';
  }

  return errors;
}

function ToFormFields(model : Model) : FormFields {
  return {
    name:         model?.name || '',
    caption:      model?.caption || '',
    description:  model?.description || '',
    releasedOn:   model?.releasedOn.substring(0, 10) || ''
  };
}

function ToModel(values : FormFields, original : Model) : Model {
  return {
    id:           original.id,
    name:         values.name,
    caption:      values.caption,
    avatar:       original.avatar,
    description:  values.description,
    releasedOn:   new Date(values.releasedOn).toISOString(),
    draft:        original.draft
  }
}

// #endregion Utilities