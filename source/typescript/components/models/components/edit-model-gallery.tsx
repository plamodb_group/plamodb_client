// #region Imports

/* React */
import React, {ChangeEvent, useContext, useRef} from 'react';

/* Material UI */
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

/* plamodb */
import {Model} from '../../../api/contracts';
import ModelsStore from '../../../stores/models';

// #endregion Imports

function EditModelGallery(props : {model : Model}) {

  // #region Hooks

  const uploadInputRef = useRef(null);
  const modelsStore = useContext(ModelsStore);

  // #endregion Hooks

  // #region Events

  function onFileSelected(event : ChangeEvent<HTMLInputElement>) {
    modelsStore.addImage(props.model.id, event.target.files[0]);
  }

  // #endregion Events

  return (
    <Grid container>

      <Grid
        container
        item
        xs      = {12}
        justify = "flex-end"
      >

        {/* Upload */}
        <Button
          type      = "submit"
          variant   = "contained"
          color     = "primary"
          onClick   = {() => uploadInputRef.current && uploadInputRef.current.click()}
        >
          Upload
        </Button>

        <input
          ref       = {uploadInputRef}
          type      = "file"
          accept    = "image/*"
          style     = {{ display: "none" }}
          onChange  = {onFileSelected}
        />

      </Grid>

    </Grid>
  );
}

export default EditModelGallery;