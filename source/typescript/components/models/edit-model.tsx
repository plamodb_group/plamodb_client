// #region Imports

/* Mobx */
import {observer} from 'mobx-react-lite';

/* React */
import React, {useContext, useEffect} from 'react';
import {useParams} from 'react-router-dom';

/* Material UI */
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';

/* plamodb */
import {Model} from '../../api/contracts';
import ModelsStore from '../../stores/models';
import EditModelAvatar from './components/edit-model-avatar';
import EditModelGallery from './components/edit-model-gallery';
import EditModelProperties from './components/edit-model-properties';

// #endregion Imports

// #region Styles

let styles = makeStyles((theme : Theme) => 
  createStyles({
    section: {
      margin: theme.spacing(2, 0),
    }
  })
);

// #endregion Styles

function EditModel() {

  // #region Hooks

  const routerParams : {id : string} = useParams();
  const modelsStore = useContext(ModelsStore);

  useEffect(
    () => {
      modelsStore.loadModel(Number(routerParams.id));
    },
    []
  );

  // #endregion Hooks

  // #region Components

  let model : Model = modelsStore.Models.find((other : Model) => other.id === Number(routerParams.id));
  const classes = styles();

  // #endregion Components

  return (
    <Box >

      {/* Toolbar */}
      <AppBar
        position    = "static"
        elevation   = {0}
        color       = "primary"
      >
        <Container maxWidth="md">
          <Toolbar disableGutters>
            <Grid container justify="flex-end">

            </Grid>
          </Toolbar>
        </Container>
      </AppBar>

      <Container maxWidth="md">

        {/* Avatar */}
        <Box className={classes.section}>
          <EditModelAvatar model={model}></EditModelAvatar>
        </Box>

        <Divider />

        {/* Images */}
        <Box className={classes.section}>
          <EditModelGallery model={model}></EditModelGallery>
        </Box>

        <Divider  />

        {/* Model Properties */}
        <Box className={classes.section}>
          <EditModelProperties model={model}></EditModelProperties>
        </Box>

      </Container>

    </Box>
  );
}

export default observer(EditModel);