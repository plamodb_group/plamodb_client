// #region Imports

/* Mobx */
import {observer} from 'mobx-react-lite';

/* React */
import React, {useContext, useEffect} from 'react';
import {useParams, Link} from 'react-router-dom';

/* Material UI */
import AppBar from '@material-ui/core/AppBar';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import BackIcon from '@material-ui/icons/ArrowBack';
import EditIcon from '@material-ui/icons/Edit';

/* plamodb */
import {Model} from '../../api/contracts';
import ModelsStore from '../../stores/models';

// #endregion Imports

// #region Styles

let styles = makeStyles({
  title: {
    flexGrow:   1
  },
  label: {
    textAlign: 'right'
  }
});

// #endregion Styles

function ViewModel() {

  // #region Hooks

  const routerParams : {id : string} = useParams();
  const modelsStore = useContext(ModelsStore);

  useEffect(
    () => {
      modelsStore.loadModel(Number(routerParams.id));
    },
    []  // Only run once.
  );

  const model : Model = modelsStore.Models.find((other : any) => other.id === Number(routerParams.id));
  
  // #endregion Hooks

  // #region Styles

  const classes = styles();

  // #endregion Styles

  // #region Events

  // #endregion Events

  return (
    <Box>

      {/* Title / Tools */}
      <AppBar 
        position    = "static" 
        elevation   = {0}
        color       = "primary"
      >
        <Container maxWidth="md">
          <Toolbar disableGutters className="tall">

            {/* Back */}
            <IconButton 
              component   = {Link} 
              to          = {'/models/'}
            >
              <BackIcon />
            </IconButton>
            
            <div className={classes.title}>
              <Box mb={-2}>
                <Typography variant="h3" color="secondary">{model?.name}</Typography>
              </Box>
              <Typography variant="subtitle1" color="secondary">{model?.caption}</Typography>
            </div>

            {/* Edit */}
            <IconButton component={Link} to={`/models/${model?.id}/edit`}>
              <EditIcon />
            </IconButton>

          </Toolbar>
        </Container>
      </AppBar>

      {/* Data */}
      <Container maxWidth="md">
        Hello, world!
      </Container>

    </Box>
  );
}

export default observer(ViewModel);