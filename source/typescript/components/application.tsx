// #region Imports

/* React */
import React from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom';

/* Material-UI */
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

/* plamodb */
import EditModel from './models/edit-model';
import ViewModel from './models/view-model';
import ListModels from './models/list-models';

// #endregion Imports

export const Application = () => {
  return (
    <Router>

      {/* Css Reset */}
      <CssBaseline></CssBaseline>

      {/* Header */}
      <AppBar 
        elevation     = {0} 
        position      = "sticky"
        color         = "secondary"
      >
        <Toolbar>
          <Typography variant="h5" noWrap>
            plamodb
          </Typography>
        </Toolbar>
      </AppBar>

      {/* Content */}

      <Switch>

        {/* Models */}
        <Route exact path="/models">
          <ListModels />
        </Route>

        {/* View Model Details */}
        <Route exact path="/models/:id">
          <ViewModel />
        </Route>

        {/* Edit Model Details */}
        <Route exact path="/models/:id/edit">
          <EditModel />
        </Route>

        {/* Default */}
        <Route path="/">
          {/* TODO: This is temporary. Should show a search page eventually. */}
          <Redirect to="/models" />
        </Route>

      </Switch>

    </Router>
  );
}