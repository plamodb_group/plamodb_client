// #region Imports

/* axios */
import axios, {AxiosInstance, AxiosResponse} from 'axios';

/* plamodb */
import {Image, Model} from './contracts';

// #endregion Imports

let endpoint : AxiosInstance = axios.create({
  baseURL:  '/api/models'
});

// #region Models

export async function getModels() : Promise<Array<Model>> {
  let response : AxiosResponse = await endpoint.get('/');
  return response.data;
}

export async function getModel(id : number) : Promise<Model> {
  let response : AxiosResponse = await endpoint.get(`/${id}`);
  return response.data;
}

export async function addModel() : Promise<Model> {
  let response : AxiosResponse = await endpoint.post('/');
  return response.data;
}

export async function updateModel(id : number, model : Model) : Promise<any> {
  let response : AxiosResponse = await endpoint.put(`/${id}`, model);
  return response.data;
}

// #endregion Models

// #region Images

export async function setAvatar(id : number, file : File) : Promise<string> {
  const formdata : FormData = new FormData();
  formdata.append('avatar', file);
  
  let response : AxiosResponse = await endpoint.post(`/${id}/avatar`, formdata);

  return response.data;
}

/**
 * Get all images of a specific model. If a model does not have any images, an
 * empty array will be returned.
 * 
 * @param id The identifier of the model to get the images for.
 */
export async function getImages(id : number) : Promise<Array<Image>> {
  let response : AxiosResponse = await endpoint.get(`${id}/images`);
  return response.data;
}

/**
 * Add an image to a model. The file will be stored on disk, alongside a smaller
 * thumbnail image that will automatically be generated.
 * 
 * @param id The identifier of the model to add an image to.
 * @param file The file containing the image.
 */
export async function addImage(id : number, file : File) : Promise<string> {
  const formdata : FormData = new FormData();
  formdata.append('image', file);

  let response : AxiosResponse = await endpoint.post(`/${id}/images`, formdata);
  return response.data;
}

// #endregion Images