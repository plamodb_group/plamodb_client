export interface Model {

  // #region Properties
  
  /**
   * Uniquely identifies this model.
   */
  id : number;

  /**
   * A human-friendly identifier for this model.
   */
  name : string;

  /**
   * Additional information about the model. Should only be one line of text.
   */
  caption : string;

  /**
   * The name of the image file used in model listings.
   */
  avatar : string;

  /**
   * A description of this model.
   */
  description : string;

  /**
   * When this model was released, in ISO8601 format.
   */
  releasedOn : string;

  /**
   * Whether or not this model should show in public listings.
   */
  draft : boolean;

  // #endregion Properties

}