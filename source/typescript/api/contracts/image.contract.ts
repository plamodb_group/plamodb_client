export interface Image {

  // #region Properties

  /**
   * Uniquely identifies this image.
   */
  id : number;

  /**
   * The filename of the image.
   */
  filename : string;

  /**
   * The filename of the thumbnail of this image.
   */
  thumbnailFilename : string;

  // #endregino Properties

}