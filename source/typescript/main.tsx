// #region Imports

/* React */
import React from 'react';
import ReactDOM from 'react-dom';

/* Material-UI */
import {ThemeProvider} from '@material-ui/core/styles';

/* plamodb */
import Theme from './theme';
import {Application} from './components/application';

// #endregion Imports

ReactDOM.render(
  <ThemeProvider theme={Theme}>
    <Application></Application>
  </ThemeProvider>,
  document.getElementById('application')
);