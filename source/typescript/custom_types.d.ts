/**
 * To allow import statements for png files to be written so that webpack can
 * find and bundle them.
 */
declare module "*.png" {
  const value: any;
  export default value;
}

/**
 * To allow import statements for sass css files.
 */
declare module '*.scss' {
  const content: {[className: string]: string};
  export = content;
}