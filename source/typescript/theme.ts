// #region Imports

/* Material-UI */
import {createMuiTheme} from '@material-ui/core/styles';

/* plamodb */
import '../styles/fonts.scss';
import '../styles/material.scss';

// #endregion Imports

const plamodbColors = {
  white:      '#e8eddf',
  yellow:     '#F5CB5C',
  dark_grey:  '#242423',
  light_grey: '#333533'
}

export default createMuiTheme({
  palette: {
    
    primary: {
      main:       plamodbColors.yellow
    },
    secondary: {
      main:       plamodbColors.dark_grey
    },

    background: {
      default:    plamodbColors.dark_grey
    },

    text: {
      primary:    plamodbColors.white,
      secondary:  '#666'
    }
  },

  typography: {
    h3: {
      fontFamily: 'Teko'
    },
    h4: {
      fontFamily: 'Teko'
    },
    subtitle1: {
      fontFamily:     'Teko',

      letterSpacing:  1.3,
      marginTop:      '-10px'
    },
    subtitle2: {
      fontFamily:     'Teko'
    }
  }
});
