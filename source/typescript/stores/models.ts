// #region Imports

/* Mobx */
import {
  action,
  makeObservable,
  observable,
  runInAction
} from 'mobx';

/* React */
import {createContext} from 'react';

/* plamodb */
import {
  addImage,
  getModel, 
  getModels,
  addModel,
  updateModel,
  setAvatar
} from '../api/models.api';
import {Model} from '../api/contracts';

// #endregion Imports

class ModelsStore {

  // #region Properties

  public Models : Array<Model> = [];

  // #endregion Properties

  // #region Life-Cycle

  /**
   * Creates a new instance of this store.
   */
  public constructor() {
    makeObservable(
      this,
      {
        Models:         observable,
        addImage:       action,
        loadModels:     action,
        loadModel:      action,
        addModel:       action,
        setAvatar:      action,
        updateModel:    action
      }
    );
  }

  // #endregion Life-Cycle

  // #region Operations
  
  /**
   * Load all the models from the back-end.
   */
  public async loadModels() : Promise<void> {
    let models : Array<Model> = await getModels();
    runInAction(() => this.Models = models);
  }

  /**
   * Load a specific model from the back-end.
   * 
   * @param modelId The identifier of the model to load.
   */
  public async loadModel(modelId : number) : Promise<void> {
    let model : Model = await getModel(modelId);
    runInAction(() => {
      this.Models = [];
      this.Models.push(model)
    });
  }

  /**
   * Adds a new model with all its properties set to defaults.
   */
  public async addModel() : Promise<Model> {
    return addModel();
  }

  /**
   * Update the properties of a specific model.
   * 
   * @param modelId The identifier of the model to update.
   * @param model The data of the model to update.
   */
  public async updateModel(modelId : number, model : Model) : Promise<void> {
    return updateModel(modelId, model);
  }

  /**
   * Set the avatar image of a model. If the model already has an avatar image,
   * that will be deleted first.
   * 
   * @param modelId The identifier of the model for which to set the image.
   * @param file The file to use as the avatar.
   */
  public async setAvatar(modelId : number, file : File) : Promise<void> {
    await setAvatar(modelId, file);
    const model : Model = await getModel(modelId);
    
    runInAction(() => {
      let index : number = this.Models.findIndex((other : any) => other.id === modelId);
      this.Models.splice(index, 1, model);
    });
  }

  /**
   * Add an image to the model. This image will show in the gallery.
   * 
   * @param modelId The identifier of the model to add the image to.
   * @param file The file containing the image.
   */
  public async addImage(modelId : number, file : File) : Promise<void> {
    await addImage(modelId, file);
  }

  // #endregion Operations

}

export default createContext(new ModelsStore());