// #region Imports

const path = require('path');
const htmlplugin = require('html-webpack-plugin');

// #endregion Imports

module.exports = {
  mode:     'development',
  entry:    path.resolve('./source/typescript/main.tsx'),
  resolve:  {
    extensions:     ['.ts', '.tsx', '.vue', '.js', '.png', '.html', '.scss']
  },
  module:   {
    rules:    [
      {
        test:     /\.(ts|tsx)?$/,
        use:      [
          {
            loader:   'ts-loader',
            options: {
              configFile:   path.resolve('./source/typescript/tsconfig.json')
            }
          }
        ]
      },
      {
        test:     /\.png?$/,
        use:      [
          {
            loader:   'file-loader'
          }
        ]
      },
      {
        test:     /\.scss?$/,
        use:      [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  output:   {
    path:           path.resolve('./distribution'),
    filename:       'application.js'
  },
  devtool:      'source-map',
  watch:        true,
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  devServer: {
    contentBase:    path.resolve('./distribution/webpack'),
    port:           8188,
    proxy: {
      '/api': 'http://localhost:8080'
    },
    historyApiFallback: true
  },
  plugins: [
    new htmlplugin({
      title:            'plamodb',
      lang:             'en-US',
      baseHref:         '/',

      template:         require('html-webpack-template'),
      bodyHtmlSnippet: '<div id="application"></div>'
    })
  ]
}